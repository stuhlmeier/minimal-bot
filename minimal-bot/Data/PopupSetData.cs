using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Threading.Tasks;
using Discord.WebSocket;

namespace Minimal.Data
{
    public class PopupSetData
    {
        private readonly SqliteDatabase _database;

        public PopupSetData(SqliteDatabase database)
        {
            _database = database;
        }

        public async Task Initialize()
        {
            using (var command = _database.NewCommand(
                @"
                create table if not exists popup_channel_set (
                    guild_id unsigned big int not null,
                    name varchar(255) not null,
                    
                    primary key (guild_id, name)
                );
                "
            )) await command.ExecuteNonQueryAsync();

            using (var command = _database.NewCommand(
                @"
                create table if not exists popup_channel_entry (
                    set_guild_id unsigned bit int not null references popup_channel_set(guild_id),
                    set_name varchar(255) not null references popup_channel_set(name),
                    channel_id unsigned bit int not null,
                    
                    unique (set_guild_id, set_name, channel_id) on conflict ignore
                );
                "
            )) await command.ExecuteNonQueryAsync();
        }

        public async Task<IEnumerable<ulong>> GetPopupSetChannelIds(ulong guildId, string popupSet,
            SQLiteTransaction transaction = null)
        {
            using var command = _database.NewCommand(
                "select channel_id from popup_channel_entry where set_guild_id = @GuildId and set_name = @SetName;",
                parameters: new Dictionary<string, object> {{"@GuildId", guildId}, {"@SetName", popupSet}}
            );
            command.Transaction = transaction;
            return (await command.ExecuteReaderAsync()).ToEnumerable(r => (ulong) r.GetInt64(0));
        }

        public async Task<bool> GetPopupSetExists(ulong guildId, string popupSet, SQLiteTransaction transaction = null)
        {
            using var command = _database.NewCommand(
                "select 1 from popup_channel_set where guild_id = @GuildId and name = @SetName;",
                parameters: new Dictionary<string, object> {{"@GuildId", guildId}, {"@SetName", popupSet}}
            );
            command.Transaction = transaction;
            using var reader = await command.ExecuteReaderAsync();
            return reader.HasRows;
        }

        public async Task<IEnumerable<SocketGuildChannel>> GetPopupSetChannels(SocketGuild guild, string popupSet,
            SQLiteTransaction transaction = null) =>
            (await GetPopupSetChannelIds(guild.Id, popupSet, transaction)).Select(guild.GetChannel)
            .Where(c => c != null);

        public async Task<IEnumerable<string>> GetPopupSets(ulong guildId, SQLiteTransaction transaction = null)
        {
            using var command = _database.NewCommand(
                "select name from popup_channel_set where guild_id = @GuildId;",
                parameters: new Dictionary<string, object> {{"@GuildId", guildId}}
            );
            command.Transaction = transaction;
            return (await command.ExecuteReaderAsync()).ToEnumerable(r => r.GetString(0));
        }

        public async Task CreatePopupSet(ulong guildId, string popupSet, SQLiteTransaction transaction = null)
        {
            using var command = _database.NewCommand(
                "insert into popup_channel_set(guild_id, name) values (@GuildId, @PopupSet);",
                parameters: new Dictionary<string, object> {{"@GuildId", guildId}, {"@PopupSet", popupSet}}
            );
            command.Transaction = transaction;
            await command.ExecuteNonQueryAsync();
        }

        public async Task DeletePopupSet(ulong guildId, string popupSet, SQLiteTransaction transaction = null)
        {
            using var command = _database.NewCommand(
                "delete from popup_channel_set where guild_id = @GuildId and name = @PopupSet;",
                parameters: new Dictionary<string, object> {{"@GuildId", guildId}, {"@PopupSet", popupSet}}
            );
            command.Transaction = transaction;
            await command.ExecuteNonQueryAsync();
        }

        public async Task AddPopupChannel(ulong guildId, string popupSet, ulong channelId,
            SQLiteTransaction transaction = null)
        {
            using var command = _database.NewCommand(
                "insert into popup_channel_entry(set_guild_id, set_name, channel_id) values (@GuildId, @PopupSet, @ChannelId);",
                parameters: new Dictionary<string, object>
                    {{"@GuildId", guildId}, {"@PopupSet", popupSet}, {"@ChannelId", channelId}}
            );
            command.Transaction = transaction;
            await command.ExecuteNonQueryAsync();
        }

        public async Task RemovePopupChannel(ulong guildId, string popupSet, ulong channelId,
            SQLiteTransaction transaction = null)
        {
            using var command = _database.NewCommand(
                "delete from popup_channel_entry where set_guild_id = @GuildId and set_name = @PopupSet and channel_id = @ChannelId;",
                parameters: new Dictionary<string, object>
                    {{"@GuildId", guildId}, {"@PopupSet", popupSet}, {"@ChannelId", channelId}}
            );
            command.Transaction = transaction;
            await command.ExecuteNonQueryAsync();
        }

        public async Task<IEnumerable<string>> GetPopupSetsFromChannel(ulong guildId, ulong channelId,
            SQLiteTransaction transaction = null)
        {
            using var command = _database.NewCommand(
                "select set_name from popup_channel_entry where set_guild_id = @GuildId and channel_id = @ChannelId;",
                parameters: new Dictionary<string, object> {{"@GuildId", guildId}, {"@ChannelId", channelId}}
            );
            command.Transaction = transaction;
            return (await command.ExecuteReaderAsync()).ToEnumerable(r => r.GetString(0));
        }

        public async Task<IEnumerable<ulong>> GetNeighborChannelIds(ulong guildId, ulong channelId,
            SQLiteTransaction transaction = null)
        {
            using var command = _database.NewCommand(
                @"
                select distinct b.channel_id from popup_channel_entry as a
                inner join popup_channel_entry as b on a.set_guild_id = b.set_guild_id and a.set_name = b.set_name and a.channel_id <> b.channel_id
                where a.set_guild_id = @GuildId and a.channel_id = @ChannelId;
                ",
                parameters: new Dictionary<string, object> {{"@GuildId", guildId}, {"@ChannelId", channelId}}
            );
            command.Transaction = transaction;
            return (await command.ExecuteReaderAsync()).ToEnumerable(r => (ulong) r.GetInt64(0));
        }

        public async Task<IEnumerable<(ulong channelId, int overlaps)>> GetPopupSetOverlaps(ulong guildId,
            SQLiteTransaction transaction = null)
        {
            using var command = _database.NewCommand(
                @"
                select channel_id, count(set_name) as overlaps from popup_channel_entry
                group by set_guild_id, channel_id;
                ",
                parameters: new Dictionary<string, object> {{"@GuildId", guildId}}
            );
            command.Transaction = transaction;
            return (await command.ExecuteReaderAsync()).ToEnumerable(r => ((ulong) r.GetInt64(0), r.GetInt32(1)));
        }

        public async Task<IEnumerable<string>> GetPopupSets(ulong guildId, ulong channelId,
            SQLiteTransaction transaction = null)
        {
            using var command = _database.NewCommand(
                "select set_name from popup_channel_entry where set_guild_id = @GuildId and channel_id = @ChannelId;",
                parameters: new Dictionary<string, object> {{"@GuildId", guildId}, {"@ChannelId", channelId}}
            );
            command.Transaction = transaction;
            return (await command.ExecuteReaderAsync()).ToEnumerable(r => r.GetString(0));
        }
    }
}