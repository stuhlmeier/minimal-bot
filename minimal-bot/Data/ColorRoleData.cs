using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Threading.Tasks;
using Discord.WebSocket;

namespace Minimal.Data
{
    public class ColorRoleData
    {
        private readonly SqliteDatabase _database;

        public ColorRoleData(SqliteDatabase database)
        {
            _database = database;
        }

        public async Task Initialize()
        {
            using var command = _database.NewCommand(
                @"
                create table if not exists color_role_entry (
                    guild_id unsigned big int not null,
                    role_id unsigned big int not null,
                    
                    unique (guild_id, role_id) on conflict ignore
                );
                "
            );
            await command.ExecuteNonQueryAsync();
        }

        public async Task<IEnumerable<ulong>> GetColorRoleIds(ulong guildId, SQLiteTransaction transaction = null)
        {
            using var command = _database.NewCommand(
                "select role_id from color_role_entry where guild_id = @GuildId;",
                parameters: new Dictionary<string, object> {{"@GuildId", guildId}}
            );
            command.Transaction = transaction;
            return (await command.ExecuteReaderAsync()).ToEnumerable(r => (ulong) r.GetInt64(0));
        }

        public async Task<IEnumerable<SocketRole>> GetColorRoles(SocketGuild guild,
            SQLiteTransaction transaction = null) =>
            (await GetColorRoleIds(guild.Id, transaction)).Select(guild.GetRole).Where(r => r != null);

        public async Task AddColorRole(ulong guildId, ulong roleId, SQLiteTransaction transaction = null)
        {
            using var command = _database.NewCommand(
                "insert into color_role_entry (guild_id, role_id) values (@GuildId, @RoleId);",
                parameters: new Dictionary<string, object>
                    {{"@GuildId", guildId}, {"@RoleId", roleId}}
            );
            command.Transaction = transaction;
            await command.ExecuteNonQueryAsync();
        }

        public async Task RemoveColorRole(ulong guildId, ulong roleId, SQLiteTransaction transaction = null)
        {
            using var command = _database.NewCommand(
                "delete from color_role_entry where guild_id = @GuildId and role_id = @RoleId;",
                parameters: new Dictionary<string, object>
                    {{"@GuildId", guildId}, {"@RoleId", roleId}}
            );
            command.Transaction = transaction;
            await command.ExecuteNonQueryAsync();
        }

        public async Task ClearColorRoles(ulong guildId, SQLiteTransaction transaction = null)
        {
            using var command = _database.NewCommand(
                "delete from color_role_entry where guild_id = @GuildId;",
                parameters: new Dictionary<string, object> {{"@GuildId", guildId}}
            );
            command.Transaction = transaction;
            await command.ExecuteNonQueryAsync();
        }
    }
}