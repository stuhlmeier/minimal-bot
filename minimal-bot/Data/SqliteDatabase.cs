using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Threading.Tasks;

namespace Minimal.Data
{
    public class SqliteDatabase : IDisposable
    {
        public string ConnectionString { get; set; }

        private SQLiteConnection _connection;

        public async Task Connect()
        {
            _connection = new SQLiteConnection(ConnectionString);
            await _connection.OpenAsync();

            ColorRoleData = new ColorRoleData(this);
            PopupSetData = new PopupSetData(this);

            await ColorRoleData.Initialize();
            await PopupSetData.Initialize();
        }

        public SQLiteCommand NewCommand(string commandText = null, bool prepare = false,
            IEnumerable<KeyValuePair<string, object>> parameters = null)
        {
            var command = new SQLiteCommand(_connection);
            if (commandText != null) command.CommandText = commandText;
            if (prepare || parameters != null)
            {
                command.Prepare();
                if (parameters != null)
                {
                    foreach (var (k, v) in parameters)
                    {
                        command.Parameters.AddWithValue(k, v);
                    }
                }
            }

            return command;
        }

        public SQLiteTransaction BeginTransaction() => _connection.BeginTransaction();

        public ColorRoleData ColorRoleData { get; private set; }
        public PopupSetData PopupSetData { get; private set; }

        public void Dispose()
        {
            _connection?.Close();
            _connection = null;
        }
    }
}