using System;

namespace Minimal
{
    public static class TextUtilities
    {
        public static string FormatException(Exception e, bool stackTrace = false)
        {
            return stackTrace
                ? $"{e.GetType().Name}: {e.Message}\n{e.StackTrace}"
                : $"{e.GetType().Name}: {e.Message}";
        }
    }
}