﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using Minimal.Commands;
using Minimal.Data;

namespace Minimal
{
    internal static class Program
    {
        private static async Task Main()
        {
            using var services = CreateProvider();

            var client = services.GetRequiredService<DiscordSocketClient>();

            client.Log += LogAsync;
            services.GetRequiredService<CommandService>().Log += LogAsync;

            await client.LoginAsync(TokenType.Bot, Environment.GetEnvironmentVariable("token"));
            await client.StartAsync();

            await services.GetRequiredService<CommandHandler>().InitializeAsync();
            await services.GetRequiredService<WelcomeHandler>().InitializeAsync();

            var database = services.GetRequiredService<SqliteDatabase>();
            database.ConnectionString = "Data Source=minimal.sqlite;Version=3;";
            await database.Connect();

            await Task.Delay(-1);
        }

        private static Task LogAsync(LogMessage message)
        {
            Console.WriteLine(message);
            return Task.CompletedTask;
        }

        private static ServiceProvider CreateProvider()
        {
            return new ServiceCollection()
                .AddSingleton<SqliteDatabase>()
                .AddSingleton<DiscordSocketClient>()
                .AddSingleton<CommandService>()
                .AddSingleton<CommandHandler>()
                .AddSingleton<WelcomeHandler>()
                .BuildServiceProvider();
        }
    }
}