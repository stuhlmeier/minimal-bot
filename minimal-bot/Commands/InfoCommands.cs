using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

namespace Minimal.Commands
{
    public class InfoCommands : ModuleBase<SocketCommandContext>
    {
        private readonly CommandService _commands;

        public InfoCommands(CommandService commands) => _commands = commands;

        [Command("info")]
        public async Task Info()
        {
            if (!(Context.User is IGuildUser guildUser)) return;
            if (!(Context.Channel is IGuildChannel guildChannel)) return;
            var self = Context.Client.CurrentUser;

            var commandNames = _commands.Commands.Select(c =>
            {
                var requiredPermissions = c.Preconditions.OfType<RequireUserPermissionAttribute>();

                // Check if this command can be used by this user
                var notAllowed = requiredPermissions.Any(p =>
                    (p.ChannelPermission != null &&
                     !guildUser.GetPermissions(guildChannel).Has(p.ChannelPermission.Value))
                    || (p.GuildPermission != null && !guildUser.GuildPermissions.Has(p.GuildPermission.Value)));

                if (notAllowed) return null;

                var commandTitle = c.Aliases.FirstOrDefault() ?? "null";

                return c.Preconditions.Any(p => p is RequireUserPermissionAttribute)
                    ? $"{commandTitle} :lock:"
                    : commandTitle;
            }).Where(c => c != null).Distinct().OrderBy(c => c);

            await ReplyAsync(embed:
                new EmbedBuilder()
                    .WithTitle("Information")
                    .WithUrl("https://gitlab.com/notiocide/minimal-bot")
                    .WithThumbnailUrl("https://i.imgur.com/RYHysMs.png")
                    .WithAuthor(self.Username, self.GetAvatarUrl())
                    .WithDescription("Minimal bot")
                    .AddField("Prefix", ",", true)
                    .AddField("Commands", string.Join("\n", commandNames))
                    .WithFooter("embot3")
                    .Build()
            );
        }
    }
}