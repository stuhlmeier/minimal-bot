using System;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace Minimal.Commands
{
    public class CommandHandler
    {
        private readonly IServiceProvider _provider;
        private readonly DiscordSocketClient _client;
        private readonly CommandService _commands;

        public CommandHandler(IServiceProvider provider, DiscordSocketClient client, CommandService commands)
        {
            (_provider, _client, _commands) = (provider, client, commands);

            _client.MessageReceived += MessageReceivedAsync;
            _commands.CommandExecuted += CommandExecutedAsync;
        }

        public async Task InitializeAsync()
        {
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), _provider);
        }

        private async Task MessageReceivedAsync(SocketMessage received)
        {
            if (!(received is SocketUserMessage message)) return;
            if (!(message.Channel is IGuildChannel)) return;
            if (message.Source != MessageSource.User) return;

            var position = 0;
            if (!message.HasCharPrefix(',', ref position)) return;

            var context = new SocketCommandContext(_client, message);
            await _commands.ExecuteAsync(context, position, _provider);
        }

        private async Task CommandExecutedAsync(Optional<CommandInfo> command, ICommandContext context, IResult result)
        {
            if (!command.IsSpecified || result.IsSuccess) return;

            await context.Channel.SendMessageAsync($"error: {result}");
        }
    }
}