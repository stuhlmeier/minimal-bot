using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Minimal.Data;

namespace Minimal.Commands
{
    [Group("popup")]
    [Alias("pop")]
    public class PopupCommands : ModuleBase<SocketCommandContext>
    {
        private readonly SqliteDatabase _database;

        public PopupCommands(SqliteDatabase database) => _database = database;

        [Command("list")]
        [RequireUserPermission(ChannelPermission.ManageChannels)]
        [RequireUserPermission(ChannelPermission.ManageRoles)]
        public async Task ListSets()
        {
            var sets = (await _database.PopupSetData.GetPopupSets(Context.Guild.Id))
                .ToList();

            if (sets.Count == 0) await ReplyAsync("there are no popup channel sets in this server");
            else await ReplyAsync($"popup channel sets:\n{string.Join("\n", sets)}");
        }

        [Command("create-set")]
        [Alias("cs")]
        [RequireUserPermission(ChannelPermission.ManageChannels)]
        [RequireUserPermission(ChannelPermission.ManageRoles)]
        public async Task CreateSet(string popupSet)
        {
            await _database.PopupSetData.CreatePopupSet(Context.Guild.Id, popupSet);
            await ReplyAsync("ok, done");
        }

        [Command("delete-set")]
        [Alias("ds")]
        [RequireUserPermission(ChannelPermission.ManageChannels)]
        [RequireUserPermission(ChannelPermission.ManageRoles)]
        public async Task DeleteSet(string popupSet)
        {
            await _database.PopupSetData.DeletePopupSet(Context.Guild.Id, popupSet);
            await ReplyAsync("ok, done");
        }

        [Command("list")]
        [RequireUserPermission(ChannelPermission.ManageChannels)]
        [RequireUserPermission(ChannelPermission.ManageRoles)]
        public async Task ListChannels(string popupSet)
        {
            using var _ = _database.BeginTransaction();

            if (!await _database.PopupSetData.GetPopupSetExists(Context.Guild.Id, popupSet, _))
            {
                await ReplyAsync($"popup channel set \"{popupSet}\" does not exist");
                return;
            }

            var channels = (await _database.PopupSetData.GetPopupSetChannels(Context.Guild, popupSet, _))
                .Select(c => MentionUtils.MentionChannel(c.Id))
                .ToList();

            if (channels.Count == 0) await ReplyAsync($"popup channel set \"{popupSet}\" is empty");
            else await ReplyAsync($"popup channels in set \"{popupSet}\":\n{string.Join("\n", channels)}");
        }

        [Command("add")]
        [RequireUserPermission(ChannelPermission.ManageChannels)]
        [RequireUserPermission(ChannelPermission.ManageRoles)]
        public async Task AddChannel(string popupSet, params SocketGuildChannel[] channels)
        {
            foreach (var channel in channels)
            {
                await _database.PopupSetData.AddPopupChannel(Context.Guild.Id, popupSet, channel.Id);
            }

            await ReplyAsync("ok, done");
        }

        [Command("remove")]
        [Alias("rm")]
        [RequireUserPermission(ChannelPermission.ManageChannels)]
        [RequireUserPermission(ChannelPermission.ManageRoles)]
        public async Task RemoveChannel(string popupSet, SocketGuildChannel channel)
        {
            await _database.PopupSetData.RemovePopupChannel(Context.Guild.Id, popupSet, channel.Id);
            await ReplyAsync("ok, done");
        }

        [Command("open")]
        [RequireUserPermission(ChannelPermission.ManageChannels)]
        [RequireUserPermission(ChannelPermission.ManageRoles)]
        public async Task OpenChannel(SocketGuildChannel channel)
        {
            using var _ = _database.BeginTransaction();

            var overlaps = (await _database.PopupSetData.GetPopupSetOverlaps(Context.Guild.Id, _))
                .Where(o => o.overlaps > 1)
                .ToList();

            if (overlaps.Count != 0)
            {
                await ReplyAsync(
                    "cannot modify popup channel state; this popup set overlaps with others.");
                await ReplyAsync(
                    $"detected overlaps:\n{string.Join("\n", overlaps.Select(o => $"{MentionUtils.MentionChannel(o.channelId)} is in {o.overlaps} sets"))}");
                return;
            }

            await ReplyAsync("ok, working...");

            foreach (var neighbor in (await _database.PopupSetData.GetNeighborChannelIds(Context.Guild.Id, channel.Id,
                    _))
                .Select(Context.Guild.GetChannel).Where(c => c != null))
            {
                var overwrite = (neighbor.GetPermissionOverwrite(Context.Guild.EveryoneRole) ?? default)
                    .Modify(viewChannel: PermValue.Deny, sendMessages: PermValue.Deny);

                await neighbor.AddPermissionOverwriteAsync(Context.Guild.EveryoneRole, overwrite);
            }

            {
                var overwrite = (channel.GetPermissionOverwrite(Context.Guild.EveryoneRole) ?? default)
                    .Modify(viewChannel: PermValue.Allow, sendMessages: PermValue.Allow);

                await channel.AddPermissionOverwriteAsync(Context.Guild.EveryoneRole, overwrite);
            }

            await ReplyAsync("ok, popup channel opened");
        }

        [Command("force-open")]
        [Alias("fo", "force")]
        [RequireUserPermission(ChannelPermission.ManageChannels)]
        [RequireUserPermission(ChannelPermission.ManageRoles)]
        public async Task ForceOpenChannel(params SocketGuildChannel[] channels)
        {
            foreach (var channel in channels)
            {
                var overwrite = (channel.GetPermissionOverwrite(Context.Guild.EveryoneRole) ?? default)
                    .Modify(viewChannel: PermValue.Allow, sendMessages: PermValue.Allow);

                await channel.AddPermissionOverwriteAsync(Context.Guild.EveryoneRole, overwrite);
            }

            await ReplyAsync("ok, channels opened");
        }

        [Command("close")]
        [RequireUserPermission(ChannelPermission.ManageChannels)]
        [RequireUserPermission(ChannelPermission.ManageRoles)]
        public async Task CloseSet(string popupSet)
        {
            using var _ = _database.BeginTransaction();

            var overlaps = (await _database.PopupSetData.GetPopupSetOverlaps(Context.Guild.Id, _))
                .Where(o => o.overlaps > 1)
                .ToList();

            if (overlaps.Count != 0)
            {
                await ReplyAsync(
                    "cannot modify popup channel state; this popup set overlaps with others.");
                await ReplyAsync(
                    $"detected overlaps:\n{string.Join("\n", overlaps.Select(o => $"{MentionUtils.MentionChannel(o.channelId)} is in {o.overlaps} sets"))}");
                return;
            }

            await ReplyAsync("ok, working...");

            foreach (var channel in (await _database.PopupSetData.GetPopupSetChannels(Context.Guild, popupSet, _)))
            {
                var overwrite = (channel.GetPermissionOverwrite(Context.Guild.EveryoneRole) ?? default)
                    .Modify(viewChannel: PermValue.Deny, sendMessages: PermValue.Deny);

                await channel.AddPermissionOverwriteAsync(Context.Guild.EveryoneRole, overwrite);
            }

            await ReplyAsync("ok, popup set closed");
        }

        [Command("close")]
        [RequireUserPermission(ChannelPermission.ManageChannels)]
        [RequireUserPermission(ChannelPermission.ManageRoles)]
        public async Task CloseSets()
        {
            using var _ = _database.BeginTransaction();

            var overlaps = (await _database.PopupSetData.GetPopupSetOverlaps(Context.Guild.Id, _))
                .Where(o => o.overlaps > 1)
                .ToList();

            if (overlaps.Count != 0)
            {
                await ReplyAsync(
                    "cannot modify popup channel state; this popup set overlaps with others.");
                await ReplyAsync(
                    $"detected overlaps:\n{string.Join("\n", overlaps.Select(o => $"{MentionUtils.MentionChannel(o.channelId)} is in {o.overlaps} sets"))}");
                return;
            }

            await ReplyAsync("ok, working...");

            foreach (var popupSet in await _database.PopupSetData.GetPopupSets(Context.Guild.Id, _))
            {
                foreach (var channel in (await _database.PopupSetData.GetPopupSetChannels(Context.Guild, popupSet, _)))
                {
                    var overwrite = (channel.GetPermissionOverwrite(Context.Guild.EveryoneRole) ?? default)
                        .Modify(viewChannel: PermValue.Deny, sendMessages: PermValue.Deny);

                    await channel.AddPermissionOverwriteAsync(Context.Guild.EveryoneRole, overwrite);
                }
            }

            await ReplyAsync("ok, all popup sets closed");
        }
    }
}