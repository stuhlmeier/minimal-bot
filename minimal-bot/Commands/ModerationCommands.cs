using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace Minimal.Commands
{
    public class ModerationCommands : ModuleBase<SocketCommandContext>
    {
        [Command("ban")]
        [RequireUserPermission(GuildPermission.BanMembers)]
        public async Task Ban(SocketGuildUser user)
        {
            await user.BanAsync();
        }

        [Command("kick")]
        [RequireUserPermission(GuildPermission.KickMembers)]
        public async Task Kick(SocketGuildUser user)
        {
            await user.KickAsync();
        }

        [Command("unban")]
        [RequireUserPermission(GuildPermission.BanMembers)]
        public async Task Unban(ulong userId)
        {
            await Context.Guild.RemoveBanAsync(userId);
            await ReplyAsync("ok, done");
        }
    }
}