using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Minimal.Data;
using Minimal.Graphics;
using ImageFormat = System.Drawing.Imaging.ImageFormat;

namespace Minimal.Commands
{
    [Group("color")]
    [Alias("c")]
    public class ColorCommands : ModuleBase<SocketCommandContext>
    {
        private readonly SqliteDatabase _database;
        private readonly ColorPreviewImage _preview = new ColorPreviewImage();

        public ColorCommands(SqliteDatabase database) => _database = database;

        [Command]
        public async Task ListColors()
        {
            var roles = (await _database.ColorRoleData.GetColorRoles(Context.Guild)).ToList();

            if (roles.Count == 0)
            {
                await ReplyAsync("there are no colors available");
                return;
            }

            await using var stream = new MemoryStream();
            (_preview.Bitmap ?? _preview.Regenerate(roles)).Save(stream, ImageFormat.Png);
            stream.Position = 0;
            await Context.Channel.SendFileAsync(stream, "colors.png", "colors:");
        }

        [Command("add")]
        [RequireUserPermission(GuildPermission.ManageRoles)]
        public async Task AddColors(params SocketRole[] roles)
        {
            foreach (var role in roles)
            {
                await _database.ColorRoleData.AddColorRole(Context.Guild.Id, role.Id);
            }

            _preview.Regenerate(roles);
            await ReplyAsync("ok, done");
        }

        [Command("remove")]
        [Alias("rm")]
        [RequireUserPermission(GuildPermission.ManageRoles)]
        public async Task RemoveColors(params SocketRole[] roles)
        {
            foreach (var role in roles)
            {
                await _database.ColorRoleData.RemoveColorRole(Context.Guild.Id, role.Id);
            }

            _preview.Regenerate(roles);
            await ReplyAsync("ok, done");
        }

        [Command("clear")]
        [Priority(1)]
        [RequireUserPermission(GuildPermission.ManageRoles)]
        public async Task ClearColors()
        {
            await _database.ColorRoleData.ClearColorRoles(Context.Guild.Id);

            _preview.Dispose();
            await ReplyAsync("ok, done");
        }

        [Command]
        public async Task SetColor(string color)
        {
            if (!(Context.User is IGuildUser guildUser)) return;

            var roles = (await _database.ColorRoleData.GetColorRoles(Context.Guild)).ToList();

            var match = roles.FirstOrDefault(r => r.Name == color);
            if (match == null)
            {
                await ReplyAsync("that color doesn't exist");
                return;
            }

            foreach (var role in roles.Where(r => guildUser.RoleIds.Contains(r.Id)))
                await guildUser.RemoveRoleAsync(role);

            await guildUser.AddRoleAsync(match);
            await ReplyAsync("ok, done");
        }
    }
}