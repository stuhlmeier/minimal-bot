using System;
using System.Linq;
using System.Threading.Tasks;
using Discord.WebSocket;
using Minimal.Data;

namespace Minimal.Commands
{
    public class WelcomeHandler
    {
        private readonly DiscordSocketClient _client;
        private readonly SqliteDatabase _database;

        private readonly Random _random = new Random();

        public WelcomeHandler(DiscordSocketClient client, SqliteDatabase database) =>
            (_client, _database) = (client, database);

        public Task InitializeAsync()
        {
            _client.UserJoined += async user =>
            {
                var roles = (await _database.ColorRoleData.GetColorRoles(user.Guild)).ToList();
                await user.AddRoleAsync(roles[_random.Next(roles.Count)]);

                await user.Guild.DefaultChannel.SendMessageAsync($"**welcome to minimal, **{user.Mention}");
            };
            _client.UserLeft += async user =>
            {
                // Don't send two messages for bans
                if ((await user.Guild.GetBansAsync()).Any(b => b.User.Id == user.Id)) return;
                await user.Guild.DefaultChannel.SendMessageAsync($"**goodbye, **{user.Mention}");
            };
            _client.UserBanned += async (user, guild) =>
            {
                await guild.DefaultChannel.SendMessageAsync($"{user}** has been banned**");
            };

            return Task.CompletedTask;
        }
    }
}