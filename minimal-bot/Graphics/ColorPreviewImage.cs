using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using Discord.WebSocket;

namespace Minimal.Graphics
{
    public class ColorPreviewImage : IDisposable
    {
        public struct PreviewOptions
        {
            public int VSpacing;
            public int HSpacing;
            public int VMargin;
            public int HMargin;
            public int FontSize;
            public int Columns;
            public Color BackgroundColor;
            public IEnumerable<string> FontFamilies;

            public static readonly PreviewOptions Default = new PreviewOptions
            {
                Columns = 4,
                VSpacing = 12,
                HSpacing = 12,
                VMargin = 6,
                HMargin = 12,
                FontSize = 32,
                BackgroundColor = Color.FromArgb(53, 57, 63),
                FontFamilies =
                    new List<string> {"Whitney Medium", "Helvetica Neue", "Helvetica", "Arial", "Microsoft Sans Serif"}
                        .AsReadOnly()
            };
        }

        public Bitmap Bitmap { get; private set; }

        private readonly PreviewOptions _options;
        private Lazy<Font> _font;

        public ColorPreviewImage(PreviewOptions? options = null)
        {
            _options = options ?? PreviewOptions.Default;
            InitializeLazyFont();
        }

        public Bitmap Regenerate(IEnumerable<SocketRole> roles)
        {
            var font = _font.Value;

            var list = roles.ToList();
            var sizes = list.Select(r => GraphicsUtilities.MeasureString(r.Name, font)).ToList();

            var width = (int) sizes.Max(s => s.Width);
            var height = (int) sizes.Max(s => s.Height);

            var rows = (list.Count + (_options.Columns - 1)) / _options.Columns;

            var bitmap = new Bitmap(
                (_options.HMargin * 2) + (width * _options.Columns) + (_options.HSpacing * (_options.Columns - 1)),
                (_options.VMargin * 2) + (height * rows) + (_options.VSpacing * (rows - 1))
            );

            using (var graphics = System.Drawing.Graphics.FromImage(bitmap))
            {
                using var brush = new SolidBrush(_options.BackgroundColor);
                graphics.FillRectangle(brush, 0, 0, bitmap.Width, bitmap.Height);
            }

            using (var graphics = System.Drawing.Graphics.FromImage(bitmap))
            {
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphics.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;

                var row = 0;
                var column = 0;
                foreach (var role in list)
                {
                    var x = _options.HMargin + (_options.HSpacing + width) * column;
                    var y = _options.VMargin + (_options.VSpacing + height) * row;

                    var roleColor = role.Color;
                    using (var brush = new SolidBrush(Color.FromArgb(roleColor.R, roleColor.G, roleColor.B)))
                    {
//                        graphics.FillRectangle(Brushes.White, x, y, width, height);
                        graphics.DrawString(role.Name, font, brush, x, y);
                    }

                    column++;
                    if (column >= _options.Columns)
                    {
                        column = 0;
                        row++;
                    }
                }
            }

            Bitmap = bitmap;
            return bitmap;
        }

        public void Dispose()
        {
            Bitmap?.Dispose();
            Bitmap = null;

            if (_font.IsValueCreated)
            {
                _font.Value.Dispose();
                InitializeLazyFont();
            }
        }

        private void InitializeLazyFont()
        {
            _font = new Lazy<Font>(() => GetFontFallback(_options, _options.FontFamilies));
        }

        private static Font GetFontFallback(PreviewOptions options, IEnumerable<string> fontFamilies)
        {
            foreach (var family in fontFamilies)
            {
                var success = false;
                var font = new Font(family, options.FontSize, FontStyle.Regular);
                try
                {
                    if (font.Name != family)
                    {
                        Console.WriteLine($"Font family {family} could not be located! Using fallback.");
                        font.Dispose();
                    }
                    else
                    {
                        success = true;
                        return font;
                    }
                }
                finally
                {
                    if (!success) font.Dispose();
                }
            }

            return new Font("Microsoft Sans Serif", options.FontSize);
        }
    }
}