using System;
using System.Drawing;

namespace Minimal.Graphics
{
    public static class GraphicsUtilities
    {
        private static readonly Bitmap _unitBitmap = new Bitmap(1, 1);

        public static System.Drawing.Graphics NullGraphics { get; } = System.Drawing.Graphics.FromHwnd(IntPtr.Zero);

        public static SizeF MeasureString(string text, Font font) => NullGraphics.MeasureString(text, font);
    }
}