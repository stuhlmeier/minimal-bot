using System;
using System.Collections.Generic;
using System.Data.Common;

namespace Minimal
{
    public static class DataUtilities
    {
        public static IEnumerable<T> ToEnumerable<T>(this DbDataReader reader, Func<DbDataReader, T> transform)
        {
            using (reader)
            {
                if (!reader.HasRows) yield break;
                while (reader.Read()) yield return transform(reader);
            }
        }
    }
}